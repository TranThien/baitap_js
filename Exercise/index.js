// bai 1
function total() {
    var salaryElement = document.getElementById("salary-day");
    var workElement = document.getElementById("work-day");

    var textElement = document.getElementById("text");
    textElement.innerHTML = salaryElement.value * workElement.value;
}

// bai 2

function press() {
    var num1Element = document.getElementById("num-1");
    var num2Element = document.getElementById("num-2");

    var num3Element = document.getElementById("num-3");

    var num4Element = document.getElementById("num-4");

    var num5Element = document.getElementById("num-5");
    var printValueElement = document.getElementById("print");

    printValueElement.innerHTML =
        (num1Element.value * 1 +
            num2Element.value * 1 +
            num3Element.value * 1 +
            num4Element.value * 1 +
            num5Element.value * 1) /
        5;
}

// /bai 3

function change() {
    var moneyVndElement = document.getElementById("money-vnd");
    var moneyUsdElement = document.getElementById("money-usd");

    var resultValue = document.getElementById("result");
    resultValue.innerHTML = (
        moneyVndElement.value * moneyUsdElement.value
    ).toFixed(3);
}

// BAI 4

function count() {
    var longElement = document.getElementById("long");
    var widthElement = document.getElementById("width");
    var visibilityValue = document.getElementById("visibility");
    var perimeterValue = (longElement.value * 1 + widthElement.value * 1) * 2;
    var areaValue = longElement.value * widthElement.value;
    visibilityValue.innerHTML = `Chu vi : ${perimeterValue} 
    Diện tích : ${areaValue}`;
}

// bai 5

function sum() {
    var numberElement = document.getElementById("number");
    var tenValue = Math.floor(numberElement.value / 10);
    var unitValue = Math.floor(numberElement.value) % 10;

    var returnValue = document.getElementById("return");
    returnValue.innerHTML = tenValue * 1 + unitValue * 1;
}